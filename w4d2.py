import sqlite3
conn = sqlite3.connect('employee.db')
cursor = conn.cursor()
cursor.execute("DROP TABLE IF EXISTS EMPLOYEE")
query = """CREATE TABLE EMPLOYEE( NAME CHAR(20),
        ID INTEGER PRIMARY KEY , SALARY FLOAT, DEPARTMENT_ID INTEGER,
         FOREIGN KEY (DEPARTMENT_ID) REFERENCES DEPARTMENTS (DEPARTMENT_ID))"""
conn.execute(query)
query = """ALTER TABLE EMPLOYEE
            ADD City CHAR(20)"""
conn.execute(query)
query = ('INSERT INTO EMPLOYEE (NAME,ID,SALARY,DEPARTMENT_ID,City) '
         'VALUES ( :NAME,:ID,:SALARY, :DEPARTMENT_ID,:City);')
params = {
        'NAME': 'Roger',
        'ID': 2,
        'SALARY': 24320,
        'DEPARTMENT_ID': 102,
        'City': 'Glasgow'
        }
conn.execute(query, params)
conn.execute("INSERT INTO EMPLOYEE(NAME,ID,SALARY,DEPARTMENT_ID,City)"
             "VALUES('Sean', 1 , 16490, 102,'Miami')")
conn.execute("INSERT INTO EMPLOYEE(NAME,ID,SALARY,DEPARTMENT_ID,City)"
             "VALUES('Dalton', 3 , 26640, 104,'Brighton')")
conn.execute("INSERT INTO EMPLOYEE(NAME,ID,SALARY,DEPARTMENT_ID,City)"
             "VALUES('Pierce', 4 , 86908, 105,'Columbia')")
conn.execute("INSERT INTO EMPLOYEE(NAME,ID,SALARY,DEPARTMENT_ID,City)"
             "VALUES('Daniel', 5 , 169087, 104,'Southampton')")

# Read the Name, ID, and Salary from the Employee table and print it.
SS = """SELECT * FROM EMPLOYEE"""
cursor.execute(SS)
rec = cursor.fetchall()
for i in rec:
    print("Name:", i[0])
    print("ID:", i[1])
    print("Salary", i[2], "\n")
# Print the details of employees whose names start with ‘j’ (or any letter input by the user)
letter = input("Check starting letter").upper()
SS = """SELECT * FROM EMPLOYEE WHERE NAME LIKE '{}%' """.format(letter)
cursor.execute(SS)
rec = cursor.fetchall()
for i in rec:
    print("Name:", i[0])
    print("ID:", i[1])
    print("Salary:", i[2])
    print("Department ID:", i[3])
    print("City:", i[4], "\n")

# Print the details of employees with ID’s inputted by the user.
iD = int(input("Check with ID"))
SS = """SELECT * FROM EMPLOYEE WHERE ID = {} """.format(iD)
cursor.execute(SS)
rec = cursor.fetchall()
for i in rec:
    print("Name:", i[0])
    print("ID:", i[1])
    print("Salary:", i[2])
    print("Department ID:", i[3])
    print("City:", i[4], "\n")

iD = int(input("Changing name with ID,Enter ID"))
nam = input("New name is:")
conn.execute("UPDATE EMPLOYEE set NAME = '{}' where ID = {}".format(nam, iD))
SS = """SELECT * FROM EMPLOYEE WHERE ID = {} """.format(iD)
cursor.execute(SS)
rec = cursor.fetchall()
for i in rec:
    print("Name:", i[0])
    print("ID:", i[1])
    print("Salary:", i[2])
    print("Department ID:", i[3])
    print("City:", i[4], "\n")


conn.commit()
conn.close()
